var http = require("http");
var url = require("url");
var io = require("socket.io");

//var error;
var rooms = [];

function start(route, handle) {
    function onRequest(request, response) {
        var pathname  = url.parse(request.url).pathname;
        var getData = "";

        console.log("Request received for " + pathname);
        
        if(url.parse(request.url).query != null)
            {
                getData = url.parse(request.url).query;
                console.log(getData);
            }
        
            request.setEncoding("utf-8");
            route(handle, pathname, response, getData);
    }

    var app = http.createServer(onRequest).listen(8888);
    console.log("Server has started");

    io = io.listen(app);

    io.sockets.on("connection", function(socket) {
        console.log("Connection established");
        socket.on("createroom", function(data){
            console.log("attempt to create room with name:" + data.name);
            var error = 0;
            rooms.forEach(function(e){
                if(e.name == data.name)
                {
                    error=1;
                }
            });
            if(error!=0)
            {
                var err = {};
                err['name'] = data.name;
                err['status'] = -1;
                io.sockets.emit("reply",err);
                error = 0;
            }
            else
            {
                var err = {};
                err['name'] = data.name;
                err['status'] = 0;
                io.sockets.emit("reply",err);
                var room_obj = {};
                room_obj['name'] = data.name;
                room_obj['link'] = "";
                room_obj['chatsock'] = socket.on(data.name+"/receive",function(data2){
                    socket.emit(data.name+"/chat",data2);
                });
                room_obj['linksock'] = socket.on(data.name+"/reqchange",function(data2){
                    socket.emit(data.name+"/linkchange",data2);
                }); 
                room_obj['getlink'] = socket.on(data.name+"/asklink",function(data2){
                    socket.emit(data.name+"/link",data.link);
                });
                rooms.push(room_obj);
            var data = {};
            data['names'] = [];
            data['names'].push(data.name);
            io.sockets.emit("roomlist",data);
            }
        });
        socket.on("getrooms",function(data){
            console.log("getrooms() called");
            var data = {};
            data['names'] = [];
            rooms.forEach(function(e){
                data['names'].push(e.name);
            });
            io.sockets.emit("roomlist",data);
        });
        /*socket.on("joinroom",function(data){
            console.log("attempt to join room");
            rooms.forEach(function(e){
                if(indexOf(e.name) !=-1)
                {
                    error=1;
                }
            });
            if(error==1)
            {
                data.status=1;
                io.sockets.emit("jsuccess", data);
            }
        });*/
    });
}

exports.start = start;
