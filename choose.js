var socket = io.connect("http://localhost");

$(document).ready(function() {
	socket.emit("getrooms", {});
	socket.on("roomlist", function(data) {
		console.log(data);
		data.names.forEach(function(n) {
			$("#list").append("<p>" + n + "</p>");
		});
	});
});
